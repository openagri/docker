# Docker

A collection of Docker resources used by AgrigateZA.

## Images

`electron.dockerfile` - a bare bones Debian slim with packages needed for building windows electron apps.
`ruby.dockerfile` - Ruby 2.7 image with postgres client, certain gems and chrome installed.
`python.dockerfile` - Python 3 image based on AWS's amazonlinux image, certain python libraries installed.
`aws-docker.dockerfile` - AWS ClI image with docker-in-docker backed in to build and push images used on AWS.

## Build & Push

Sign in to registry:

```
docker login registry.gitlab.com/openagri/docker -u <username> -p <token>
```

Replace x.x.x with the latest version incremented by one. We use a semver naming scheme, e.g.  
0.0.x - bug fix  
0.x.0 - feature (backwards compatible)  
x.0.0 - feature / major change (backwards incompatible)

```
docker build -t registry.gitlab.com/openagri/docker:ruby-x.x.x -f ruby.dockerfile ./
docker push registry.gitlab.com/openagri/docker:ruby-x.x.x

docker build -t registry.gitlab.com/openagri/docker:electron-x.x.x -f electron.dockerfile ./
docker push registry.gitlab.com/openagri/docker:electron-x.x.x

docker build -t registry.gitlab.com/openagri/docker:python-x.x.x -f python.dockerfile ./
docker push registry.gitlab.com/openagri/docker:python-x.x.x

docker build -t registry.gitlab.com/openagri/docker:aws-docker-x.x.x -f aws-docker.dockerfile ./
docker push registry.gitlab.com/openagri/docker:aws-docker-x.x.x
```

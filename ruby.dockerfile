FROM ruby:3.1.2

# Update package lists and install Node.js 18
RUN apt-get update && \
    curl -fsSL https://deb.nodesource.com/setup_18.x | bash - && \
    apt-get install -y nodejs

# Install other dependencies and tools
RUN apt-get install -y \
    ca-certificates \
    fonts-liberation \
    libappindicator3-1 \
    libasound2 \
    libatk-bridge2.0-0 \
    libatk1.0-0 \
    libcups2 \
    libdbus-1-3 \
    libgbm1 \
    libgdk-pixbuf2.0-0 \
    libgtk-3-0 \
    libnspr4 \
    libnss3 \
    libx11-xcb1 \
    libxcomposite1 \
    libxdamage1 \
    libxrandr2 \
    libxss1 \
    python3 \
    python3-pip \
    wget \
    xdg-utils

# Verify Node.js and npm installations
RUN npm install -g npm@latest @puppeteer/browsers

# Install Ruby gems we use that take long to install
RUN gem install bundler --no-document && \
    gem install sassc -v 2.4.0 && \
    gem install pg -v 1.4.2

RUN curl https://cli-assets.heroku.com/install.sh | sh

RUN pip3 install --upgrade pip && \
    pip3 --no-cache-dir install --upgrade awscli

# Install chrome + chrome driver
ENV CHROME_VERSION=131.0.6778.87
ENV CHROME_DIR="/opt/chrome/linux-$CHROME_VERSION/chrome-linux64"
ENV DRIVER_DIR="/opt/chromedriver/linux-$CHROME_VERSION/chromedriver-linux64"
RUN npx @puppeteer/browsers install chrome@$CHROME_VERSION --path /opt
RUN npx @puppeteer/browsers install chromedriver@$CHROME_VERSION --path /opt
ENV PATH="${PATH}:${CHROME_DIR}:${DRIVER_DIR}"
FROM amazon/aws-cli

#install docker-in-docker to use the docker CLI
RUN amazon-linux-extras install docker

# aws-cli image's entrypoint is set to the aws command. this removes that 
ENTRYPOINT [""]
# AWS based image bundled with python & heroku cli
FROM public.ecr.aws/amazonlinux/amazonlinux:latest

#Amazon Linux package manager Yum
RUN yum upgrade -y && \
  yum install -y git tar python3 && \
  curl https://cli-assets.heroku.com/install.sh | sh

RUN pip3 install boto3 sentry-sdk
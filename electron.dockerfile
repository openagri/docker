FROM node:12-buster-slim

RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    apt-utils ca-certificates \
    libglib2.0-0 libnss3 libgdk-pixbuf2.0-0 \
    libgtk-3-0 libx11-xcb1 libxtst6 libxss1 libasound2 \
    wine wine32 libxcb-dri3-0 libdrm2 libgbm-dev git \
    unixodbc unixodbc-dev python3 build-essential wget curl

RUN wget http://dl.min.io/client/mc/release/linux-amd64/mc && \
    chmod +x mc